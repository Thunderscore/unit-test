# Unit Test

## Installation
1. Copy the repository
2. Install dependencies
3. Start the application

## Terminal Commands
```
git clone https://gitlab.com/Thunderscore/unit-test
cd .\unit-test
npm i
```

## Running Program
`npm run start`

`npm run test`

## Console

![console image](console_img.png)